var searchData=
[
  ['idlestate',['IdleState',['../class_idle_state.html',1,'']]],
  ['idlestate_2ecs',['IdleState.cs',['../_idle_state_8cs.html',1,'']]],
  ['ienemystate',['IEnemyState',['../interface_i_enemy_state.html',1,'']]],
  ['ienemystate_2ecs',['IEnemyState.cs',['../_i_enemy_state_8cs.html',1,'']]],
  ['ignorecollision',['IgnoreCollision',['../class_ignore_collision.html',1,'']]],
  ['ignorecollision_2ecs',['IgnoreCollision.cs',['../_ignore_collision_8cs.html',1,'']]],
  ['increaselife',['increaseLife',['../class_life_manager.html#a8bb0588341820c87aa7660886e117238',1,'LifeManager']]],
  ['initialize',['Initialize',['../class_knife.html#a748d17f3f0dc85378fe8364b468a0285',1,'Knife.Initialize()'],['../classstat.html#a25e97119769208828bd4e706cf35ec84',1,'stat.Initialize()']]],
  ['inmeleerange',['InMeleeRange',['../class_enemy.html#a01150b6bff8b984fe01606c0d9a3faa9',1,'Enemy']]],
  ['instance',['Instance',['../class_player.html#aff08b341c76fb1c361d87556dc552975',1,'Player']]],
  ['inthrowrange',['InThrowRange',['../class_enemy.html#a385315a59c9ebd2501e3fa957d9223e2',1,'Enemy']]],
  ['isdead',['IsDead',['../class_character.html#a5b3fd8f625e060df3d051cdefcbb9975',1,'Character.IsDead()'],['../class_enemy.html#a756e20b818dab19026267dc3f7fc8843',1,'Enemy.IsDead()'],['../class_player.html#aca51d3ce8d9afaaa84547b0e525e0833',1,'Player.IsDead()']]],
  ['ispaused',['isPaused',['../class_pause_menu.html#a3d0a43917cf3e488852b47d1b755b377',1,'PauseMenu']]]
];
