﻿using UnityEngine;
using System.Collections;

public delegate void DeadEventHandler();

public class Player : Character {

	private Vector2 startPos;

	private static Player instance;

	public event DeadEventHandler Dead;

	[SerializeField]
	private stat healthStat;

	public float shotDelay;
	private float shotDelayCounter;

	public AudioSource Audio1; 
	public AudioSource Audio2;
	public AudioSource Audio3;
	public AudioSource Audio4;
	public AudioSource Audio5;
	public AudioSource Audio6;
		public static Player Instance
	{
		get
		{
			if (instance == null) {
				instance = GameObject.FindObjectOfType<Player>();
			}
			return instance;
		}

	}

	[SerializeField]
	private Transform[] groundPoints; 
	[SerializeField]
	private float radius;

	[SerializeField]
	private LayerMask whatIsGround;

	[SerializeField]
	private bool airControl;
	[SerializeField]
	private float jumpForce;
	private SpriteRenderer spriteRenderer;

	private bool immortal=false;
	[SerializeField]
	private float immortalTime;



	float lastTimet;
	float lastTimea;

	public Rigidbody2D MyRigidbody { get; set;}

	public bool Jump { get; set;}

	public bool OnGround { get; set;}

//	public static void PlayerHealth( int MaxPlayerHealth)
//	{
//		MaxPlayerHealth=health;
//		MaxPlayerHealth=50;
//	}

	public override bool IsDead
	{
		get
		{ 
			if (healthStat.CurrentVal <= 0) {
								OnDead ();
			}

			return healthStat.CurrentVal<= 0;
		}
	}


	public override void Start () {
//		PlayerHealth (50);
		//int playerHealth=health;
		//PlayerHealth
		lastTimet = Time.fixedTime;
		lastTimet = Time.fixedTime;

		//health=50;

		base.Start ();

		startPos = transform.position;
		spriteRenderer = GetComponent<SpriteRenderer> ();
		MyRigidbody = GetComponent<Rigidbody2D> ();
		//LifeManager.lifeCounter = startingLives;
		healthStat.Initialize();
	}

	void Update(){   

		if (Input.GetKeyDown (KeyCode.X) && (Time.fixedTime - lastTimet) > 1.0f) 
		{
			MyAnimator.SetTrigger ("throw");
			Audio6.Play ();
			lastTimet = Time.fixedTime;
			shotDelayCounter = shotDelay;

		}
		if (Input.GetKey (KeyCode.X)) 
		{
			shotDelayCounter -= Time.deltaTime;

			if (shotDelayCounter <= 0) 
			{
				shotDelayCounter = shotDelay;
				MyAnimator.SetTrigger ("throw");
				lastTimet = Time.fixedTime;
				Audio6.Play ();
			}
				
		}

		if( Input.GetKeyDown(KeyCode.Z) && (Time.fixedTime - lastTimea) > 1.0f)
		{
			MyAnimator.SetTrigger ("attack");
			Audio5.Play ();	
			lastTimea = Time.fixedTime;
		}

		if (!TakingDamage&&!IsDead) {
			if (transform.position.y <= -10f) {
				//LifeManager.lifeCounter = LifeManager.lifeCounter - 1;
				LifeManager.decreaseLife ();
								Death();
			
			}

			HInput();
		}


	}                                     //update calls depends on the frame speed of the system so called very often
	void FixedUpdate () {
		if (!TakingDamage&&!IsDead) {
			float horizontal = Input.GetAxis ("Horizontal");

			OnGround = IsGrounded ();
			Movement (horizontal);
			Flip (horizontal);
			Layers ();
		}


	}                                      //fixed update calls depends on the time and called after some interval, used for all physics function.

	public void OnDead()
	{
		if (Dead != null) {
			
						Dead ();
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.transform.tag == "movingPlatform") {
			transform.parent = other.transform;
		}
	}
		
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.transform.tag == "movingPlatform") {
			transform.parent = null;
		}
	}

	private void Flip(float horizontal){
		if(horizontal>0&&!faceRight||horizontal<0&&faceRight){
			changeDirection ();
		}
	}                                     //flip function flips our character when we press the arrow keys.




	private void HInput(){
		
		if( Input.GetKeyDown(KeyCode.Space) )
		{
						MyAnimator.SetTrigger ("jump");
						if(OnGround)
			              Audio3.Play ();
			
		}

	}                                                  //controls the input to the player through keys by changing the bool value



	private void  Movement(float horizontal)
	{
		if (MyRigidbody.velocity.y < 0) {
			MyAnimator.SetBool ("land", true);
		}
		if (!Attack && (OnGround || airControl)) {
			MyRigidbody.velocity = new Vector2 (horizontal * movementSpeed, MyRigidbody.velocity.y);
		}
		if (Jump && MyRigidbody.velocity.y == 0) {
			MyRigidbody.AddForce (new Vector2 (0, jumpForce));
		}
		MyAnimator.SetFloat ("speed", Mathf.Abs (horizontal));
	}


	private bool IsGrounded(){
		if(MyRigidbody.velocity.y<=0){
			foreach (Transform point in groundPoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll (point.position, radius, whatIsGround);

				for( int i=0 ;i<colliders.Length; i++){
					if(colliders[i].gameObject!=gameObject)
					{

						return true;
					}
				}
			}
		}
		return false;
	}                                                   //function to check whether player is on the ground or not

	private void Layers(){

		if (!OnGround) {
			MyAnimator.SetLayerWeight (1, 1);
		}
		else {
			MyAnimator.SetLayerWeight (1, 0);
		}          

	}                                                      //changing the layers

	public override void ThrowKinfe(int value)
	{
		if (!OnGround && value == 1 || OnGround && value == 0) 
		{
			base.ThrowKinfe (value);
		}

	}

	private IEnumerator IndicateImmortal()
	{
		while (immortal) {
			spriteRenderer.enabled = false;
			yield return new WaitForSeconds (.1f);
			spriteRenderer.enabled = true ;
			yield return new WaitForSeconds (.1f);
		}
	}

	public override IEnumerator TakeDamage()
	{
		if (!immortal &&health>0) {
			healthStat.CurrentVal -= 10;
			if (!IsDead) {
				MyAnimator.SetTrigger ("damage");
				Audio4.Play ();
				immortal = true;
				StartCoroutine (IndicateImmortal());
				yield return new WaitForSeconds (immortalTime);
				immortal = false;
			}
			else {

				LifeManager.decreaseLife ();
				MyAnimator.SetLayerWeight (1, 0);

				MyAnimator.SetTrigger ("die");
				Audio2.Play ();
			}
		}
	
	}

	public override void Death()
	{
		
		Audio1.Play();
	//	LifeManager.lifeCounter = LifeManager.lifeCounter - 1;
		MyRigidbody.velocity = Vector2.zero;
		MyAnimator.SetTrigger ("idle");
		healthStat.CurrentVal = healthStat.MaxVal;
		transform.position = startPos;
	}
    
}
