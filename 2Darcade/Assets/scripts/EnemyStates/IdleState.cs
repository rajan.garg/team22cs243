﻿using UnityEngine;
using System.Collections;

public class IdleState : IEnemyState {

	private Enemy enemy;

	private float idleTimer;     //time we were in idle
	private float idleDuration;     //time it will stay in idle

	public void Enter(Enemy enemy)
	{
		idleDuration = UnityEngine.Random.Range (2, 8);
		this.enemy = enemy;
	}

	public void Execute()
	{
		Idle ();
		Debug.Log ("idleing");
		if (enemy.Target != null) {
			enemy.ChangeState (new PatrolState ());
		}
	}

	public void Exit()
	{

	}

	public void OnTriggerEnter(Collider2D other)
	{
		if (other.tag == "knife") {
			enemy.Target = Player.Instance.gameObject;		
		}
	}

	private void Idle()
	{
		enemy.MyAnimator.SetFloat ("speed", 0);
		idleTimer += Time.deltaTime ;

		if (idleTimer >= idleDuration) 
		{
			enemy.ChangeState (new PatrolState ());
		}
	}

}
