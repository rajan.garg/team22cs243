﻿using UnityEngine;
using System.Collections;

public class Enemy : Character {
	
	[SerializeField]
	public Vector2 startPos;

	private IEnemyState currentState;
	public GameObject Target{ get; set; }

	[SerializeField]
	private float meleeRange;

	[SerializeField]
	private float throwRange;



	public AudioSource EAudio1; 
	public AudioSource EAudio2;

	public bool InMeleeRange
	{
		get
		{ 
			if (Target!=null) 
			{
				return Vector2.Distance (transform.position, Target.transform.position) <= meleeRange;
			}
			return false;
		}
	}

	public bool InThrowRange
	{
		get
		{ 
			if (Target!=null) 
			{
				return Vector2.Distance (transform.position, Target.transform.position) <= throwRange;
			}
			return false;
		}
	}

	public override bool IsDead
	{
		get
		{ 
			return health <= 0;
		}
	}

	public override void Start ()
	{
		//health=20;
		base.Start ();
		startPos = transform.position;
		Player.Instance.Dead += new DeadEventHandler (Removetarget);
		ChangeState (new IdleState ());
	}


	void Update () {
		if (!IsDead) 
		{
			if (!TakingDamage) {
				currentState.Execute ();
			}

			LookAtTarget ();
		}

	}

	public void Removetarget()
	{
		Target = null;
		ChangeState (new PatrolState ());
	}

	private void LookAtTarget()
	{
		if (Target != null) 
		{
			float xDir = Target.transform.position.x - transform.position.x;
			if (xDir < 0 && faceRight || xDir > 0 && !faceRight) {
				changeDirection ();
			}
		}
	}

	public void ChangeState(IEnemyState newState)
	{
		if (currentState != null) {
			currentState.Exit ();
		}
		currentState = newState;
		currentState.Enter (this);
	}

	public void Move()
	{
		if (!Attack) {
			 
				MyAnimator.SetFloat ("speed", 1);
				transform.Translate (GetDirection () * (movementSpeed * Time.deltaTime));
			
		}

	}

	public Vector2 GetDirection()
	{
		return faceRight ? Vector2.right : Vector2.left;
	}

	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D (other);
		currentState.OnTriggerEnter (other);
	}

	public override IEnumerator TakeDamage()
	{
		health -= 10;
		if (!IsDead) {
			MyAnimator.SetTrigger ("damage");
			EAudio1.Play ();
		}
		else {
				MyAnimator.SetTrigger("die");
			EAudio2.Play ();
			yield return null;
		}
		//GetComponent<AudioSource> ().Play ();
	}

	public override void Death()
	{
		MyAnimator.ResetTrigger ("die");
	//	MyAnimator.SetTrigger ("idle");

	//	health = 20;
	//	transform.position = startPos;

		if (gameObject == null)
		{ 
			Destroy (gameObject);
		}
	}
}
