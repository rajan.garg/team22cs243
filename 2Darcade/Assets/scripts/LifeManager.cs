﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour {

	public int startingLives;
	public static int lifeCounter;
	Text theText;
	public static int temp;
	public string gameOverLoad;

	// Use this for initialization
	void Start () {

		theText = GetComponent <Text> ();
		if(Score.score==0)
		lifeCounter = startingLives;

	}



	// Update is called once per frame
	void Update () {
		theText.text = "x " + lifeCounter;
		if (lifeCounter <= 0)
		{

			SceneManager.LoadScene(gameOverLoad);
		}
	}

	public static void decreaseLife()
	{
	 lifeCounter -= 1;

	}

	public static void increaseLife()
	{
		lifeCounter += 1;
	}
}
