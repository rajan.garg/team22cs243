﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {

	private bool playerZone;
	public string levelLoad;
	public string levelTag;
	public AudioSource AudioExit;

	void Start()
	{
		playerZone = false;
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.W) && playerZone) {
			AudioExit.Play ();
			//LifeManager.increaseLife ();
			PlayerPrefs.SetInt (levelTag, 1);
			SceneManager.LoadScene (levelLoad);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			playerZone = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			playerZone = false;
		}
	}
}
